package com.ybear.mvp;

import android.os.Bundle;

import androidx.annotation.Nullable;

public interface IFragmentArguments extends ICallArguments {
    /**
     * Fragment传递参数给FragmentActivity
     * @param args      参数
     */
    void setArgumentsToActivity(@Nullable Bundle args);
}
