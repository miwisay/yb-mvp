package com.ybear.mvp.view;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.util.Printer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.util.Consumer;
import androidx.viewbinding.ViewBinding;

import com.ybear.mvp.BaseVP;
import com.ybear.mvp.IFragmentArguments;
import com.ybear.mvp.ViewLife;
import com.ybear.mvp.annotations.Model;
import com.ybear.mvp.handler.Handler;
import com.ybear.mvp.handler.HandlerCallback;
import com.ybear.mvp.util.MvpAnn;
import com.ybear.mvp.view.bind.BindHelper;
import com.ybear.mvp.view.bind.IViewBindSelf;

import java.util.concurrent.Callable;

@Deprecated
public abstract class MvpFragment<VB extends ViewBinding> extends Fragment implements BaseVP,
        IViewBindSelf<VB>, IViewSelf<MvpFragment<VB>>, IFragmentArguments, Handler, HandlerCallback {
    @Model
    private ViewLife<BaseVP> mLife;
    private final BindHelper<VB> mBind = new BindHelper<>( this );

    public MvpFragment() {
        super();
        //实例被Model注解的成员变量
        MvpAnn.instanceMvpAnn( this );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mLife.onAttach( this );
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLife.onCreate( this, savedInstanceState );
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
//        View v = super.onCreateView( inflater, container, savedInstanceState) ;
        View view = mBind.bindView( null, inflater, container );
        mLife.onCreateView( this, savedInstanceState );
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //实例当前注解BindView的成员变量
        MvpAnn.instanceBindAll( this );
        mLife.onActivityCreate( this, savedInstanceState );
    }

    @Override
    public void onStart() {
        super.onStart();
        mLife.onStart( this );
    }

    @Override
    public void onResume() {
        super.onResume();
        mLife.onResume( this );
    }

    @Override
    public void onPause() {
        super.onPause();
        mLife.onPause( this );
    }

    @Override
    public void onStop() {
        super.onStop();
        mLife.onStop( this );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLife.onDestroyView( this );
        mBind.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLife.onDestroy( this );
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mLife.onDetach( this );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLife.onRequestPermissionsResult( this, requestCode, permissions, grantResults );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLife.onActivityResult( this, requestCode, resultCode, data );
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mLife.onSaveInstanceState( this, outState );
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mLife.onHiddenChanged( this, hidden );
    }

    @Override
    public void finish() { mLife.finishOfFragment( this ); }

    @Override
    public MvpFragment<VB> getThis() { return this; }

    @NonNull
    @Override
    public VB getViewBinding() { return mBind.getViewBinding(); }

    @Override
    public Context getApplicationContext() {
        return mLife.getApplicationContext( getActivity() );
    }

    @Nullable
    @Override
    public final <V extends View> V findViewById(int id) {
        return mLife.findViewById( this, id );
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    public Fragment findFragmentById( int id ) {
        return getChildFragmentManager().findFragmentById(  id );
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        post( () -> onCallArguments( args ) );
    }

    /**
     * 传递参数给Activity
     * @param args  参数
     */
    @Override
    public void setArgumentsToActivity(@Nullable Bundle args) {
        mLife.setArgumentsToActivity( this, args );
    }

    /**
     * 从Activity处接收到的参数
     * @param args  参数
     */
    @Override
    public void onCallArguments(@Nullable Bundle args) { }

    /**
     * 监听view(Activity)的生命周期状态
     * V层的活动状态发生改变时会调用该接口，这样P层就可以实时知道V层的状态。
     * @param status    活动状态
     */
    @Override
    public void onLifeStatus(int status, @Nullable Bundle b) { }

    /**
     * 获取当前Activity 或者 Fragment 的当前生命周期状态
     * @return  {@link com.ybear.mvp.annotations.LifeStatus}
     */
    @Override
    public int getLifeStatus() { return mLife.getLifeStatus(); }

    @Nullable
    @Override
    public Window getWindow() { return getActivity() == null ? null : getActivity().getWindow(); }

    /* Handler */
    @NonNull
    @Override
    public android.os.Handler getOsHandler() { return getHandler().getOsHandler(); }
    @NonNull
    @Override
    public final Handler getHandler() { return mLife.getHandlerOfFragment( getActivity(), this ); }
    @Override
    public boolean post(Runnable r) { return getHandler().post( r ); }
    @Override
    public boolean post(Runnable r, long delayMillis) {
        return getHandler().postDelayed( r, delayMillis );
    }
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public boolean post(Runnable r, @Nullable Object token, long delayMillis) {
        return getHandler().postDelayed( r, token, delayMillis );
    }
    @Override
    public boolean postDelayed(Runnable r, long delayMillis) {
        return getHandler().postDelayed( r, delayMillis );
    }
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public boolean postDelayed(@NonNull Runnable r, @Nullable Object token, long delayMillis) {
        return getHandler().postDelayed( r, token, delayMillis );
    }
    @Override
    public boolean postAtFrontOfQueue(@NonNull Runnable r) {
        return getHandler().postAtFrontOfQueue( r );
    }
    @Override
    public boolean postAtTime(@NonNull Runnable r, long uptimeMillis) {
        return getHandler().postAtTime( r, uptimeMillis );
    }
    @Override
    public boolean postAtTime(@NonNull Runnable r, @Nullable Object token, long uptimeMillis) {
        return getHandler().postAtTime( r, token, uptimeMillis );
    }

    @Override
    public <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult) {
        return getHandler().postAsync( executeCall, resultCall, postResult );
    }

    @Override
    public <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult) {
        return getHandler().postAsync( executeCall, resultCall, postResult );
    }

    @Override
    public <T> boolean postDelayedAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis) {
        return getHandler().postDelayedAsync( executeCall, resultCall, postResult, delayMillis );
    }

    @Override
    public <T> boolean postDelayedAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis) {
        return getHandler().postDelayedAsync( executeCall, resultCall, postResult, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis) {
        return getHandler().postAsync( executeCall, resultCall, postResult, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis) {
        return getHandler().postAsync( executeCall, resultCall, postResult, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall) {
        return getHandler().postAsync( executeCall, resultCall );
    }

    @Override
    public <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall) {
        return getHandler().postAsync( executeCall, resultCall );
    }

    @Override
    public <T> boolean postDelayedAsync(Callable<T> executeCall, Consumer<T> resultCall, long delayMillis) {
        return getHandler().postDelayedAsync( executeCall, resultCall, delayMillis );
    }

    @Override
    public <T> boolean postDelayedAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, long delayMillis) {
        return getHandler().postDelayedAsync( executeCall, resultCall, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, long delayMillis) {
        return getHandler().postAsync( executeCall, resultCall, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, long delayMillis) {
        return getHandler().postAsync( executeCall, resultCall, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Runnable executeCall) {
        return getHandler().postAsync( executeCall );
    }

    @Override
    public <T> boolean postDelayedAsync(Runnable executeCall, long delayMillis) {
        return getHandler().postDelayedAsync( executeCall, delayMillis );
    }

    @Override
    public <T> boolean postAsync(Runnable executeCall, long delayMillis) {
        return getHandler().postAsync( executeCall, delayMillis );
    }

    @Override
    public boolean sendMessage(@NonNull Message msg) { return getHandler().sendMessage( msg ); }
    @Override
    public boolean sendEmptyMessage(int what) { return getHandler().sendEmptyMessage( what ); }
    @Override
    public boolean sendEmptyMessageDelayed(int what, long delayMillis) {
        return getHandler().sendEmptyMessageDelayed( what, delayMillis );
    }
    @Override
    public boolean sendEmptyMessageAtTime(int what, long uptimeMillis) {
        return getHandler().sendEmptyMessageAtTime( what, uptimeMillis );
    }
    @Override
    public boolean sendMessageDelayed(@NonNull Message msg, long delayMillis) {
        return getHandler().sendMessageDelayed( msg, delayMillis );
    }
    @Override
    public boolean sendMessageAtTime(@NonNull Message msg, long uptimeMillis) {
        return getHandler().sendMessageAtTime( msg, uptimeMillis );
    }
    @Override
    public boolean sendMessageAtFrontOfQueue(@NonNull Message msg) {
        return getHandler().sendMessageAtFrontOfQueue( msg );
    }

    @Override
    public boolean hasMessages(int what) { return getHandler().hasMessages( what ); }
    @Override
    public boolean hasMessages(int what, @Nullable Object object) {
        return getHandler().hasMessages( what, object );
    }

    @Override
    public void removeMessages(int what) {
        getHandler().removeMessages( what );
    }
    @Override
    public void removeMessages(int what, @Nullable Object object) {
        getHandler().removeMessages( what, object );
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public boolean hasCallbacks(@NonNull Runnable r) { return getHandler().hasCallbacks( r ); }

    @Override
    public void removeCallbacks(@NonNull Runnable r) { getHandler().removeCallbacks( r ); }
    @Override
    public void removeCallbacks(@NonNull Runnable r, @Nullable Object token) {
        getHandler().removeCallbacks( r, token );
    }

    @Override
    public void removeCallbacksAndMessages(@Nullable Object token) {
        getHandler().removeCallbacksAndMessages( token );
    }

    @NonNull
    @Override
    public Message obtainMessage() { return getHandler().obtainMessage(); }
    @NonNull
    @Override
    public Message obtainMessage(int what) { return getHandler().obtainMessage( what ); }
    @NonNull
    @Override
    public Message obtainMessage(int what, @Nullable Object obj) {
        return getHandler().obtainMessage( what, obj );
    }
    @NonNull
    @Override
    public Message obtainMessage(int what, int arg1, int arg2) {
        return getHandler().obtainMessage( what, arg1, arg2 );
    }
    @NonNull
    @Override
    public Message obtainMessage(int what, int arg1, int arg2, @Nullable Object obj) {
        return getHandler().obtainMessage( what, arg1, arg2, obj );
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public android.os.Handler createAsyncForOs(@NonNull Looper looper) {
        return android.os.Handler.createAsync( looper );
    }
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public android.os.Handler createAsyncForOs(@NonNull Looper looper,
                                               @NonNull android.os.Handler.Callback callback) {
        return android.os.Handler.createAsync( looper, callback );
    }

    @Override
    public void dump(@NonNull Printer pw, @NonNull String prefix) { getHandler().dump( pw, prefix ); }

    @NonNull
    @Override
    public Looper getLooper() { return getHandler().getLooper(); }

    @Override
    public boolean handleMessage(@NonNull Message msg) { return false; }
    @Override
    public void dispatchMessage(@NonNull Message msg) {}
}
