package com.ybear.mvp.view.bind;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

public interface IViewBindSelf<VB extends ViewBinding> {
    void setContentView(View view);

    VB onBindingView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container);

    void initView(@NonNull VB binding);

    @NonNull
    VB getViewBinding();
}
