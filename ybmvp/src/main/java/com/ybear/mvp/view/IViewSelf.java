package com.ybear.mvp.view;

public interface IViewSelf<S> {
    /**
     获取自身，等同于 this
     @return    this
     */
    S getThis();
}
