package com.ybear.mvp.view;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.ybear.mvp.util.MvpAnn;

public class MvpApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext( base );
        MultiDex.install( this );
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MvpAnn.instanceMvpAnn( this );
    }
}