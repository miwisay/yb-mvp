package com.ybear.mvp.event;

import android.view.KeyEvent;
import android.view.MotionEvent;

public interface OnEventCallback {
    void onTouchEvent(MotionEvent ev);

    void dispatchTouchEvent(MotionEvent ev);

    void dispatchKeyEvent(KeyEvent ev);

    void dispatchKeyShortcutEvent(KeyEvent ev);

    void dispatchTrackballEvent(MotionEvent ev);

    void dispatchGenericMotionEvent(MotionEvent ev);
}
