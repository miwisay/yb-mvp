package com.ybear.mvp;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.Window;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.mvp.handler.Handler;

public interface BaseVP extends /*ICallArguments, */OnLifeStatus {
    /**
     获取V层的Application{@link Context}
     @return    {@link Context}
     */
    Context getApplicationContext();

    /**
     获取V层的{@link Context}
     @return    {@link Context}
     */
    @Nullable
    Context getContext();

    /**
     获取V层的{@link Activity}
     @return    {@link Activity}
     */
    @Nullable
    Activity getActivity();

    /**
     获取{@link Window}
     @return    {@link Window}
     */
    @Nullable
    Window getWindow();

    /**
     * 获取V层的资源管理器
     * @return  Activity
     */
    @Nullable
    Resources getResources();

    @Nullable
    <T extends View> T findViewById(@IdRes int id);

    @NonNull
    Handler getHandler();

    /**
     * 结束Activity 或者 Fragment
     */
    void finish();
}
