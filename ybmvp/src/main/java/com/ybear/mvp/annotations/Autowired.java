package com.ybear.mvp.annotations;

import com.ybear.mvp.model.MvpModel;
import com.ybear.mvp.presenter.MvpPresenter;
import com.ybear.mvp.view.MvpActivity;
import com.ybear.mvp.view.MvpAppCompatActivity;
import com.ybear.mvp.view.MvpFragmentActivity;
import com.ybear.mvp.view.fragment.MvpFragment;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自动实例化对象
 * 成员变量：实例化当前成员
 * （不支持带参构造，推荐init()的方式）
 *
 * 实例范围：继承{@link MvpModel}
 *         继承{@link MvpPresenter}
 *         继承{@link MvpActivity}
 *         继承{@link MvpAppCompatActivity}
 *         继承{@link com.ybear.mvp.view.MvpFragment}
 *         继承{@link MvpFragment}
 *         继承{@link MvpFragmentActivity}
 *         构造器中调用过 {@link com.ybear.mvp.util.MvpAnn#instanceMvpAnn(Object)}
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Autowired {
}
