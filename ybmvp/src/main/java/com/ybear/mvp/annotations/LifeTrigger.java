package com.ybear.mvp.annotations;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ybear.mvp.annotations.LifeTrigger.ACTIVITY_RESULT;
import static com.ybear.mvp.annotations.LifeTrigger.REQUEST_PERMISSIONS_RESULT;
import static com.ybear.mvp.annotations.LifeTrigger.SAVE_INSTANCE_STATE;
import static com.ybear.mvp.annotations.LifeTrigger.SAVE_INSTANCE_STATE_2;

/**
 * Activity 或 Fragment 等页面的生命周期触发器
 */
@Retention(RetentionPolicy.SOURCE)
public @interface LifeTrigger {
    String KEY_REQUEST_CODE = "request_code";
    String KEY_RESULT_CODE = "result_code";
    String KEY_PERMISSIONS = "permissions";
    String KEY_GRANT_RESULTS = "grant_results";
    String KEY_HIDDEN = "hidden";
    String KEY_INTENT = "intent";
    String KEY_BUNDLE = "bundle";
    String KEY_PERSISTABLE_BUNDLE = "persistable_bundle";

    /**
     * {@link android.app.Activity#onRequestPermissionsResult(int, String[], int[])}
     */
    int REQUEST_PERMISSIONS_RESULT = 30;

    /**
     * {@link android.app.Activity##onActivityResult(int, int, Intent)}
     */
    int ACTIVITY_RESULT = 31;

    /**
     * {@link android.app.Activity##onSaveInstanceState(Bundle)}
     */
    int SAVE_INSTANCE_STATE = 32;

    /**
     * {@link android.app.Activity#onSaveInstanceState(Bundle, PersistableBundle)}
     */
    int SAVE_INSTANCE_STATE_2 = 33;

    /**
     * {@link androidx.fragment.app.Fragment#onHiddenChanged(boolean)}
     * {@link android.app.Fragment#onHiddenChanged(boolean)}
     */
    int HIDDEN_CHANGED = 34;
}
