package com.ybear.mvp.annotations;

import androidx.annotation.IdRes;
import androidx.fragment.app.FragmentManager;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 绑定传入的IdRes资源到注解的成员变量
 * 绑定范围：成员变量必须继承或间接继承{@link androidx.fragment.app.Fragment} 或 {@link android.app.Fragment}
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BindFragment {
    /**
     * 与{@link FragmentManager#findFragmentById(int)} 一致的参数
     * @return  IdRes
     */
    @IdRes int value();
}