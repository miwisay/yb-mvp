package com.ybear.mvp.annotations;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ybear.mvp.annotations.LifeStatus.ACTIVITY_CREATED;
import static com.ybear.mvp.annotations.LifeStatus.ATTACH;
import static com.ybear.mvp.annotations.LifeStatus.CREATE;
import static com.ybear.mvp.annotations.LifeStatus.CREATE_VIEW;
import static com.ybear.mvp.annotations.LifeStatus.DESTROY;
import static com.ybear.mvp.annotations.LifeStatus.DESTROY_VIEW;
import static com.ybear.mvp.annotations.LifeStatus.DETACH;
import static com.ybear.mvp.annotations.LifeStatus.NEW_INTENT;
import static com.ybear.mvp.annotations.LifeStatus.PAUSE;
import static com.ybear.mvp.annotations.LifeStatus.RESTART;
import static com.ybear.mvp.annotations.LifeStatus.RESUME;
import static com.ybear.mvp.annotations.LifeStatus.START;
import static com.ybear.mvp.annotations.LifeStatus.STOP;

/**
 * Activity 或 Fragment 等页面的生命周期
 */
@IntDef({ NEW_INTENT, ATTACH, CREATE, CREATE_VIEW, ACTIVITY_CREATED, START, RESTART,
        RESUME, PAUSE, STOP, DESTROY_VIEW, DESTROY, DETACH })
@Retention(RetentionPolicy.SOURCE)
public @interface LifeStatus {
    int NEW_INTENT = 1;
    int ATTACH = 10;
    int CREATE = 11;
    int CREATE_VIEW = 12;
    int ACTIVITY_CREATED = 13;
    int START = 14;
    int RESTART = 15;
    int RESUME = 16;
    int PAUSE = 17;
    int STOP = 18;
    int DESTROY_VIEW = 19;
    int DESTROY = 20;
    int DETACH = 21;
}
