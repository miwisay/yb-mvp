package com.ybear.mvp.annotations;

import com.ybear.mvp.model.MvpModel;
import com.ybear.mvp.presenter.MvpPresenter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 实例化继承{@link MvpModel}的类
 *
 * 实例范围：继承{@link MvpModel}
 *         继承{@link MvpPresenter}
 *         所有Activity 和 所有Fragment
 * @see #value() 默认为共享Model（允许多个持有者共享一个Model），
 * 使用 {@link ModelType#NEW_MODEL} 时，会重新创建一个新的Model，并且不会参与内存共享（正常的实例对象）
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Model {
    @ModelType
    int value() default ModelType.SHARED;
}