package com.ybear.mvp.annotations;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ybear.mvp.annotations.ModelType.NEW_MODEL;
import static com.ybear.mvp.annotations.ModelType.SHARED;

/**
 * Model层的实例类型
 */
@IntDef({ SHARED, NEW_MODEL })
@Retention(RetentionPolicy.SOURCE)
public @interface ModelType {
    int SHARED = 0;     //共享模式，多个持有者持有一个Model
    int NEW_MODEL = 1;  //实例模式，常规的实例对象。
}
