package com.ybear.mvp.model;

import com.ybear.mvp.util.MvpAnn;

public class MvpModel {
    public MvpModel() {
        //实例被Model注解的成员变量
        MvpAnn.instanceMvpAnn( this );
        onCreateModel();
    }

    void release() {
        onDestroyModel();
        //移除当前存在的所有Model
        ModelManage.get().removeModelAll( this );
    }

    public void onCreateModel() {}
    public void onDestroyModel() {}
}
