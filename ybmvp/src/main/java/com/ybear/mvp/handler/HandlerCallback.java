package com.ybear.mvp.handler;

import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;

public interface HandlerCallback extends Handler.Callback {
    boolean handleMessage(@NonNull Message msg);
    void dispatchMessage(@NonNull Message msg);
}
