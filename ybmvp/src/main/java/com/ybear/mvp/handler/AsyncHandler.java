package com.ybear.mvp.handler;

import androidx.core.util.Consumer;

import java.util.concurrent.Callable;

public interface AsyncHandler {
    /**
     * 异步Handler处理（通过Return返回数据）
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post回调
     * @param <T>               数据源
     * @return                  post结果
     */
    <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult);
    /**
     * 异步Handler处理（通过回调返回数据）
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post回调
     * @param <T>               数据源
     * @return                  post结果
     */
    <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult);

    /**
     * 异步Handler处理（通过Return返回数据） - 延迟调用
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post返回。默认：true
     * @param <T>               数据源
     * @return                  post结果
     */
    <T> boolean postDelayedAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis);
    /**
     * 异步Handler处理（通过回调返回数据） - 延迟调用
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post返回。默认：true
     * @param <T>               数据源
     * @return                  post结果
     */
    <T> boolean postDelayedAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis);

    <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis);
    <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, boolean postResult, long delayMillis);

    <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall);
    <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall);

    <T> boolean postDelayedAsync(Callable<T> executeCall, Consumer<T> resultCall, long delayMillis);
    <T> boolean postDelayedAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, long delayMillis);

    <T> boolean postAsync(Callable<T> executeCall, Consumer<T> resultCall, long delayMillis);
    <T> boolean postAsync(Consumer<Consumer<T>> executeCall, Consumer<T> resultCall, long delayMillis);

    /**
     * 异步Handler处理 - 此方法不支持post返回
     * @param executeCall       处理的异步内容
     * @param <T>               数据源
     * @return                  post结果
     */
    <T> boolean postAsync(Runnable executeCall);
    <T> boolean postDelayedAsync(Runnable executeCall, long delayMillis);
    <T> boolean postAsync(Runnable executeCall, long delayMillis);
}
