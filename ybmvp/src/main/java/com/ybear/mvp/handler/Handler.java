package com.ybear.mvp.handler;

import android.os.Build;
import android.os.Looper;
import android.os.Message;
import android.util.Printer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public interface Handler extends AsyncHandler {
    android.os.Handler getOsHandler();
    Handler getHandler();

    boolean post(Runnable r);
    boolean post(Runnable r, long delayMillis);
    @RequiresApi(api = Build.VERSION_CODES.P)
    boolean post(Runnable r, @Nullable Object token, long delayMillis);
    boolean postDelayed(Runnable r, long delayMillis);
    @RequiresApi(api = Build.VERSION_CODES.P)
    boolean postDelayed(@NonNull Runnable r, @Nullable Object token, long delayMillis);
    boolean postAtFrontOfQueue(@NonNull Runnable r);
    boolean postAtTime(@NonNull Runnable r, long uptimeMillis);
    boolean postAtTime(@NonNull Runnable r, @Nullable Object token, long uptimeMillis);

    boolean sendMessage(@NonNull Message msg);
    boolean sendEmptyMessage(int what);
    boolean sendEmptyMessageDelayed(int what, long delayMillis);
    boolean sendEmptyMessageAtTime(int what, long uptimeMillis);
    boolean sendMessageDelayed(@NonNull Message msg, long delayMillis);
    boolean sendMessageAtTime(@NonNull Message msg, long uptimeMillis);
    boolean sendMessageAtFrontOfQueue(@NonNull Message msg);

    boolean hasMessages(int what);
    boolean hasMessages(int what, @Nullable Object object);
    void removeMessages(int what);
    void removeMessages(int what, @Nullable Object object);

    @RequiresApi(api = Build.VERSION_CODES.Q)
    boolean hasCallbacks(@NonNull Runnable r);
    void removeCallbacks(@NonNull Runnable r);
    void removeCallbacks(@NonNull Runnable r, @Nullable Object token);

    void removeCallbacksAndMessages(@Nullable Object token);

    @NonNull
    Message obtainMessage();
    @NonNull
    Message obtainMessage(int what);
    @NonNull
    Message obtainMessage(int what, @Nullable Object obj);
    @NonNull
    Message obtainMessage(int what, int arg1, int arg2);
    @NonNull
    Message obtainMessage(int what, int arg1, int arg2, @Nullable Object obj);

    @RequiresApi(api = Build.VERSION_CODES.P)
    android.os.Handler createAsyncForOs(@NonNull Looper looper);
    @RequiresApi(api = Build.VERSION_CODES.P)
    android.os.Handler createAsyncForOs(@NonNull Looper looper,
                                        @NonNull android.os.Handler.Callback callback);

    void dump(@NonNull Printer pw, @NonNull String prefix);
    @NonNull
    Looper getLooper();
}