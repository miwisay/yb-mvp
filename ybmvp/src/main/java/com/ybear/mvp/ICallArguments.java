package com.ybear.mvp;

import android.os.Bundle;

import androidx.annotation.Nullable;

public interface ICallArguments {
    void onCallArguments(@Nullable Bundle args);
}
