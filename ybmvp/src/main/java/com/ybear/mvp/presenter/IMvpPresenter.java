package com.ybear.mvp.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.mvp.BaseVP;
import com.ybear.mvp.OnViewLifeListener;
import com.ybear.mvp.handler.Handler;
import com.ybear.mvp.handler.HandlerCallback;
import com.ybear.mvp.view.IViewSelf;
import com.ybear.mvp.view.MvpViewable;

/**
 * P层必须实现的接口
 *
 * @param <V>
 */
interface IMvpPresenter<V extends MvpViewable> extends BaseVP, IViewSelf<MvpPresenter<V>>,
        OnViewLifeListener , Handler, HandlerCallback {
    /**
     * 获取绑定的view
     * @return  返回V层
     */
    @Nullable
    V getView();

    /**
     * 获取绑定的view
     * @throws NullPointerException 通过调用 {@link IMvpPresenter#getView()}，如果返回null，则会抛空异常
     * @return  返回V层
     */
    @NonNull
    V getViewOrThrow() throws NullPointerException;

    /**
     * 获取绑定的view
     * @param call          返回V层
     * @param h             Handler
     * @param duration      延迟毫秒
     */
    void viewCall(@NonNull Call<V> call, @Nullable Handler h, int duration);

    /**
     * 获取绑定的view
     * @param   call    返回V层
     * @param   h       Handler
     */
    void viewCall(@NonNull Call<V> call, @Nullable Handler h);

    /**
     * 获取绑定的view
     * @param call          返回V层
     * @param duration      延迟毫秒
     */
    void viewCall(@NonNull Call<V> call, int duration);

    /**
     * 获取绑定的view
     * @param   call    返回V层
     */
    void viewCall(@NonNull Call<V> call);

    /**
     * 获取绑定的View，不使用Handler回调
     * @param call      返回V层
     */
    void viewCallNotHandler(@NonNull Call<V> call);
}
