package com.ybear.mvp.presenter;

import androidx.annotation.NonNull;

import com.ybear.mvp.view.MvpViewable;

public interface Call<V extends MvpViewable> {
    void onCall(@NonNull V view);
}