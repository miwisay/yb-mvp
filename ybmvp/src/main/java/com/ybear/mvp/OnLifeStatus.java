package com.ybear.mvp;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.ybear.mvp.annotations.LifeStatus;

public interface OnLifeStatus {
    /**
     * 监听view(Activity)的生命周期状态
     * V层的活动状态发生改变时会调用该接口，这样P层就可以实时知道V层的状态。
     * @param status    活动状态
     */
    void onLifeStatus(@LifeStatus int status, @Nullable Bundle b);

    /**
     * 获取当前Activity 或者 Fragment 的当前生命周期状态
     * @return  {@link com.ybear.mvp.annotations.LifeStatus}
     */
    @LifeStatus
    int getLifeStatus();
}
