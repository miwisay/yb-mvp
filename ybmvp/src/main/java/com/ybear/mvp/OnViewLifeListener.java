package com.ybear.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.mvp.annotations.LifeTrigger;
import com.ybear.mvp.event.OnEventCallback;

public interface OnViewLifeListener extends OnEventCallback, OnLifeStatus/*, ICallArguments*/ {
    void onLifeNewIntent(Intent intent);
    void onLifeAttach();
    void onLifeCreate(@Nullable Bundle savedInstanceState);
    void onLifeCreateView(@Nullable Bundle savedInstanceState);
    void onLifeActivityCreate(@Nullable Bundle savedInstanceState);
    void onLifeStart();
    void onLifeReStart();
    void onLifeResume();
    void onLifePause();
    void onLifeStop();
    void onLifeDestroyView();
    void onLifeDestroy();
    void onLifeDetach();

    @LifeTrigger
    int getTriggerStatus();
    void onTriggerStatus(@LifeTrigger int trigger, @Nullable Bundle b);

    void onTriggerRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults);
    void onTriggerActivityResult(int requestCode, int resultCode, @Nullable Intent data);
    void onTriggerSaveInstanceState(@NonNull Bundle outState);
    void onTriggerSaveInstanceState(@NonNull Bundle outState,
                                    @Nullable PersistableBundle outPersistentState);
    void onTriggerHiddenChanged(boolean hidden);
}
