# yb-MVP
[![](https://jitpack.io/v/com.gitee.miwisay/yb-mvp.svg)](https://jitpack.io/#com.gitee.miwisay/yb-mvp)

#### 介绍

![架构图](https://images.gitee.com/uploads/images/2021/0204/174827_a6e57334_1814150.jpeg "架构图.jpg")

### 依赖包
```
    android {
        defaultConfig {
            //启用分包
            multiDexEnabled = true
        }
        //Google ViewBinding
        buildFeatures {
           viewBinding true
        }
    }
    dependencies {
        //yb-mvp框架包
        api 'com.gitee.miwisay:yb-mvp:v2.2'
    }
```

##### Activity
```
public class MainActivity extends MvpAppCompatActivity<XXXMainBinding> implements MainView {
    @Presenter
    private MainPresenter mPresenter;
    
    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        
        //如果需要Handler，可以这样使用 ↓↓↓
        testHandler();
    }
    
    /**
     * 编译完成后，XXXMainBinding会自动生成
     * XXXMainBinding会根据layout布局名称命名。
     * eg:act_test_mvp_main -> ActTestMvpMainBinding
     */
    @Override
    public XXXMainBinding onBindingView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return XXXMainBinding.inflate( inflater );
    }
    
    /**
     初始化View的默认数据
     @param binding     {@link androidx.viewbinding.ViewBinding}
     */
    @Override
    public void initView(@NonNull XXXMainBinding binding) {
        super.initView( binding );
        /* 可以定义一些基本设置 */
        //XXX为layout布局中定义的控件id。eg:act_test_main_tv_text -> actTestMainTvText
        //binding.actTestMainTvText.setTextSize( 26 );
        //binding.actTestMainTvText.setGravity( Gravity.CENTER );
        binding.XXX.setTextSize( 26 );
        binding.XXX.setGravity( Gravity.CENTER );
    }

    /**
     MainView 定义的接口
     @param s       回调结果
     */
    @Override
    public void onText( String s ) {
        //XXX为layout布局中定义的控件id。eg:act_test_main_tv_text -> actTestMainTvText
        //getViewBinding().actTestMainTvText.setText( s );
        getViewBinding().XXX.setText( s );
        Log.d( "TAG", s );
    }
    
    /**
     * Handler
     */
     private void testHandler() {
        Message msg = obtainMessage();
        msg.what = 100;
        msg.obj = getClass().getSimpleName();
        sendMessage( msg );
        post( () -> Log.d(  "TAG", "handler post.(main looper)") );
        postAsync( () -> Log.d(  "TAG", "handler postAsync.(thread)") );
        postAsync(() -> {
            //子线程
            Thread.sleep( 6000L );
            Log.d(  "TAG", "handler postAsync.call.(thread 1)");
            return "Test String 1";
        }, s -> {
            //主线程
            Log.d( "TAG", "handler postAsync.accept.(main looper 1), s:" + s );
        });
        postAsync(consumer -> {
            //子线程
            new Thread(() -> {
                try {
                    Thread.sleep( 6000L );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                consumer.accept( "Test String 2" );
                Log.d(  "TAG", "handler postAsync.call.(thread 2)");
            }).start();
        }, (Consumer<String>) s -> {
            //主线程
            Log.d( "TAG", "handler postAsync.accept.(main looper 2), s:" + s );
        });
    }
    @Override
    public boolean handleMessage(@NonNull Message msg) {
        Log.d( "TAG", String.format( "handleMessage -> Activity -> what:%s, obj:%s", msg.what, msg.obj ) );
        //如果不需要Fragment接收handlerMessage, 就注释掉super.handleMessage(msg);
        return super.handleMessage(msg);
    }
}
```

##### View
```
/**
 MainActivity 的 MvpViewable
 */
public interface MainView extends MvpViewable {
    void onText(String s);
}
```

##### Presenter
```
public class MainPresenter extends MvpPresenter<MainView> {
    @Model(ModelType.SHARED)
    private Model1 mModel1;             //共享Model
    @Model
    private Model1 mModel1_1;           //共享Model
    @Model
    private Model2 mModel2;             //共享Model
    @Model(ModelType.NEW_MODEL)
    private Model2 mModel2_2;           //重新实例的Model

    public MainPresenter( @NonNull MainView view ) {
        super( view );
        //mModel1 和 mModel1_1 为共享Model，当 mModel1 内部发生改变时， mModel1_1 也会跟着改变

        //mModel2_2 重新实例的Model，可以理解为 new了一个 Model2，与 mModel2 互不干预

        //在实际开发中，mModel1 和 mModel1_1 可以在不同的类中实现共享。
        //例如：MainPresenter、OtherPresenter 两个 Presenter 都持有 Model1
        
        /* 回调结果。Handler.post提交 */
        viewCall(v -> {
            //回调处理结果
            v.onText( mModel1.getText() );
        });

        mModel2.queryText(s -> {
            viewCall( v -> {
                //回调处理结果
                v.onText( s );
            });
        })


        /* 回调结果。同步提交 */
        MainView v = getView();
        if( v != null ) v.onText( mModel1.getText() );

        mModel2.queryText(s -> {
            MainView v = getView();
            if( v != null ) v.onText( s );
        })
    }

    /**
     Activity 或 Fragment 的生命周期
     @param status      生命周期
                        {@link LifeStatus#CREATE}、{@link LifeStatus#PAUSE}...
     @param b           传入的参数
     */
    @Override
    public void onLifeStatus( @LifeStatus int status, @Nullable Bundle b ) {
        super.onLifeStatus( status, b );
    }

    /**
     Activity 或 Fragment 创建时
     */
    @Override
    public void onLifeCreate( @Nullable Bundle savedInstanceState ) {
        super.onLifeCreate( savedInstanceState );
    }

    @Override
    public void onLife...() {}
}
```

##### Model
```
public class Model1 extends MvpModel {
    /**
     Model创建时会调用
     */
    @Override
    public void onCreateModel() {
        super.onCreateModel();
    }

    /**
     Model销毁时会调用
     */
    @Override
    public void onDestroyModel() {
        super.onDestroyModel();
    }

    public String getText() {
        return "Model1 -> Hello World!";
    }
}

public class Model2 extends MvpModel {
    /**
     Model创建时会调用
     */
    @Override
    public void onCreateModel() {
        super.onCreateModel();
    }

    /**
     Model销毁时会调用
     */
    @Override
    public void onDestroyModel() {
        super.onDestroyModel();
    }

    public void queryText(Consumer<String> call) {
        if( call != null ) call.accept( "Model2 -> Hello World!" );
    }
}
```