package com.ybear.mvp.demo.model;

import android.util.Log;

import androidx.core.util.Consumer;

import com.ybear.mvp.model.MvpModel;

public class TestQueryModel extends MvpModel{
    private String mQueryText;

    /**
     Model被创建时
     */
    @Override
    public void onCreateModel() {
        super.onCreateModel();
        mQueryText = "Init Text";
        Log.e("Model",
                "Name:" + getClass().getSimpleName() + " | " +
                "Hash:" + getClass().hashCode()
        );
    }

    /**
     Model被销毁时
     */
    @Override
    public void onDestroyModel() {
        super.onDestroyModel();
        cleanQuery();
    }

    public void cleanQuery() { mQueryText = null; }

    public void queryText(long ms, Consumer<String> call) {
        if( call == null ) return;
        //线程读取
        new Thread( () -> {
            try { Thread.sleep( ms ); }catch(InterruptedException ignored) { }
            if( mQueryText == null ) mQueryText = "Hello World!";
            call.accept( mQueryText );
        } ).start();
    }
}
