package com.ybear.mvp.demo.presenter;

import androidx.annotation.NonNull;

import com.ybear.mvp.annotations.Model;
import com.ybear.mvp.annotations.ModelType;
import com.ybear.mvp.demo.model.TestQueryModel;
import com.ybear.mvp.demo.view.TestMainView;
import com.ybear.mvp.presenter.MvpPresenter;

public class TestMainPresenter extends MvpPresenter<TestMainView>{
    //全局共享的Model
    @Model
    private TestQueryModel mQueryModel;
    //重新实例的Model
    @Model(ModelType.NEW_MODEL)
    private TestQueryModel mNewQueryModel;

    public TestMainPresenter(@NonNull TestMainView view) {
        super( view );
    }

    /**
     这里主要展示的是，共享Model 和 实例Model 的区别
     被标记为 {@link ModelType#NEW_MODEL} 的Model只会作用在当前Presenter中

     而 {@link TestMainPresenter#mQueryModel}调用了 {@link TestQueryModel#cleanQuery()} 之后
     其他类调用依旧展示清除后的文本
     */
    public void queryText() {
        //通过Model查询
        mQueryModel.queryText( 2000, s -> {
            //回调给Activity
            viewCall( v -> v.onShowText( "QueryModel:" + s ) );
            //清除默认的“Init Text”文本
            mQueryModel.cleanQuery();
        } );

        //通过Model查询
        mNewQueryModel.queryText( 3000, s -> {
            //回调给Activity
            viewCall( v -> v.onShowAddText( "NewQueryModel:" + s ) );
        } );
    }
}
