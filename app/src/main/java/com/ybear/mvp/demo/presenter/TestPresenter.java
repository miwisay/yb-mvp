package com.ybear.mvp.demo.presenter;

import androidx.annotation.NonNull;

import com.ybear.mvp.annotations.Model;
import com.ybear.mvp.demo.model.TestQueryModel;
import com.ybear.mvp.demo.view.TestView;
import com.ybear.mvp.presenter.MvpPresenter;

public class TestPresenter extends MvpPresenter<TestView>{
    //全局共享的Model
    @Model
    private TestQueryModel mQueryModel;

    public TestPresenter(@NonNull TestView view) {
        super( view );
    }

    /**
     这里的Model是共享的，同时 {@link TestMainPresenter} 中调用了 {@link TestQueryModel#cleanQuery()}
     这里的结果为清除后的文本 “Hello World!”
     */
    public void queryText() {
        //通过Model查询
        mQueryModel.queryText( 4000, s -> {
            //回调给Activity
            viewCall( v -> v.onShowText( s ) );
        } );
    }
}
