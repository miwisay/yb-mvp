package com.ybear.mvp.demo.view;

import com.ybear.mvp.view.MvpViewable;

public interface TestMainView extends MvpViewable{
    void onShowText(String s);
    void onShowAddText(String s);
}
