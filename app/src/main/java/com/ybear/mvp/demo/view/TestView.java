package com.ybear.mvp.demo.view;

import com.ybear.mvp.view.MvpViewable;

public interface TestView extends MvpViewable{
    void onShowText(String s);
}
