package com.ybear.mvp.demo.ui.activity;

import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentTransaction;

import com.ybear.mvp.annotations.Presenter;
import com.ybear.mvp.demo.databinding.ActTestMvpMainBinding;
import com.ybear.mvp.demo.presenter.TestMainPresenter;
import com.ybear.mvp.demo.ui.fragment.TestFragment;
import com.ybear.mvp.demo.view.TestMainView;
import com.ybear.mvp.view.MvpAppCompatActivity;

/**
 主入口
 */
public class TestMainActivity extends MvpAppCompatActivity<ActTestMvpMainBinding> implements TestMainView {
    @Presenter
    private TestMainPresenter mPresenter;

//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate( savedInstanceState );
//        //通过onBindingView(...)代替此方法
////        setContentView( R.layout.act_test_mvp_main );
//    }

    /**
     * 编译完成后，ActTestMvpMainBinding会自动生成
     * ActTestMvpMainBinding会根据layout布局名称命名。
     * eg:act_test_mvp_main -> ActTestMvpMainBinding
     */
    @Override
    public ActTestMvpMainBinding onBindingView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return ActTestMvpMainBinding.inflate( inflater );
    }

    /**
     初始化View的默认数据
     @param binding     {@link androidx.viewbinding.ViewBinding}
     */
    @Override
    public void initView(@NonNull ActTestMvpMainBinding binding) {
        super.initView( binding );
        /* 可以定义一些基本设置 */
        binding.actTestMainTvText.setTextSize( 26 );
        binding.actTestMainTvText.setGravity( Gravity.CENTER );

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add( getViewBinding().getRoot().getId(), new TestFragment() );
        ft.commitAllowingStateLoss();

        mPresenter.queryText();

        testHandler();
    }

    @Override
    public void onShowText(String s) {
        getViewBinding().actTestMainTvText.setText( s );
    }

    @Override
    public void onShowAddText(String s) {
        getViewBinding().actTestMainTvText.append( "\n" + s );
    }


    /**
     * Handler
     */
    private void testHandler() {
        Message msg = obtainMessage();
        msg.what = 100;
        msg.obj = getClass().getSimpleName();
        sendMessage( msg );
        post( () -> Log.d(  "TAG", "handler post.(main looper)") );
        postAsync( () -> Log.d(  "TAG", "handler postAsync.(thread)") );
        postAsync(() -> {
            //子线程
            Thread.sleep( 6000L );
            Log.d(  "TAG", "handler postAsync.call.(thread 1)");
            return "Test String 1";
        }, s -> {
            //主线程
            Log.d( "TAG", "handler postAsync.accept.(main looper 1), s:" + s );
        });
        postAsync(consumer -> {
            //子线程
            new Thread(() -> {
                try {
                    Thread.sleep( 6000L );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                consumer.accept( "Test String 2" );
                Log.d(  "TAG", "handler postAsync.call.(thread 2)");
            }).start();
        }, (Consumer<String>) s -> {
            //主线程
            Log.d( "TAG", "handler postAsync.accept.(main looper 2), s:" + s );
        });
    }
    @Override
    public boolean handleMessage(@NonNull Message msg) {
        Log.d( "TAG", String.format( "handleMessage -> Activity -> what:%s, obj:%s", msg.what, msg.obj ) );
        //如果不需要Fragment接收handlerMessage, 就注释掉super.handleMessage(msg);
        return super.handleMessage(msg);
    }
}
