package com.ybear.mvp.demo.ui.fragment;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.mvp.annotations.Presenter;
import com.ybear.mvp.demo.databinding.FragTestMvpFragmentBinding;
import com.ybear.mvp.demo.presenter.TestPresenter;
import com.ybear.mvp.demo.view.TestView;
import com.ybear.mvp.view.fragment.MvpFragment;

public class TestFragment extends MvpFragment<FragTestMvpFragmentBinding> implements TestView {
    @Presenter
    private TestPresenter mPresenter;

//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        //通过onBindingView(...)代替此方法
////        return LayoutInflater.from( getContext() ).inflate( R.layout.frag_test_mvp_fragment, container );
//        return super.onCreateView( inflater, container, savedInstanceState );
//    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );
        //默认文本
        onShowText( ( "This is Fragment" ) );
        //查询文本
        mPresenter.queryText();

        Message msg = obtainMessage();
        msg.what = 101;
        msg.obj = getClass().getSimpleName();
        sendMessage( msg );
    }

    @Override
    public FragTestMvpFragmentBinding onBindingView(@NonNull LayoutInflater inflater,
                                                      @Nullable ViewGroup container) {
        return FragTestMvpFragmentBinding.inflate( inflater, container, false );
    }

    /**
     初始化View的默认数据
     @param binding     {@link androidx.viewbinding.ViewBinding}
     */
    @Override
    public void initView(@NonNull FragTestMvpFragmentBinding binding) {
        binding.fragTestMvpFragmentTvText.setTextSize( 30 );
    }

    @Override
    public void onShowText(String s) {
        getViewBinding().fragTestMvpFragmentTvText.setText( s );
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {
        Log.d( "TAG", String.format( "handleMessage -> Fragment -> what:%s, obj:%s", msg.what, msg.obj ) );
        return super.handleMessage(msg);
    }
}
